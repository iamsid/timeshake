package com.example.ist.geofencing.roomdb;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = RoomDBConstants.TABLE_NAME)
public class GeofenceBean {

    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo(name = "latitude")
    Double latitude;

    @ColumnInfo(name = "longitude")
    Double longitude;

    public GeofenceBean(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
