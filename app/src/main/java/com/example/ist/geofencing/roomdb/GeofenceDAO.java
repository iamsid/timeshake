package com.example.ist.geofencing.roomdb;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface GeofenceDAO {

    @Query("Select * from "+RoomDBConstants.TABLE_NAME)
    List<GeofenceBean> getAll();

    @Insert
    void insert(GeofenceBean geofenceBean);

    @Query("DELETE FROM "+RoomDBConstants.TABLE_NAME)
    void delete();
}
