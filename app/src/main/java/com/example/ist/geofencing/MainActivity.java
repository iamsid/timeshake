package com.example.ist.geofencing;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ist.geofencing.roomdb.AppDatabase;
import com.example.ist.geofencing.roomdb.GeofenceBean;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout llGeofenceLatLongsList;
    private Button btnAddGeofence;
    private final int STATUS_CODE = 1;

    private String TAG = "MainActivity";

    NotificationManager notificatioManager;
    private String CHANNEL_ID = "MyChannelId";
    private int NOTIFICATION_ID = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        llGeofenceLatLongsList = findViewById(R.id.lat_longs_list_layout);
        btnAddGeofence = findViewById(R.id.add_geofence_btn);
        btnAddGeofence.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.add_geofence_btn)
            startActivityForResult(new Intent(this,MapsActivity.class),STATUS_CODE);

        sendNotification("MyEvent");
    }

    void sendNotification(String event){
        Intent notificationIntent = new Intent(getApplicationContext(),MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setContentTitle("Geofence")
                .setContentText(" "+event +" trigerred")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(notificationPendingIntent);

        // Creating and sending Notification
        notificatioManager=(NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );

        createNotificationChannel();

        notificatioManager.notify(NOTIFICATION_ID,mBuilder.build());

    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            String description= "description";
            CharSequence name = "Siddhant";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,name,importance);
            channel.setDescription(description);
            notificatioManager.createNotificationChannel(channel);
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


}
