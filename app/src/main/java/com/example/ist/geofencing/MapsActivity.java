package com.example.ist.geofencing;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.ist.geofencing.roomdb.AppDatabase;
import com.example.ist.geofencing.roomdb.GeofenceBean;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener, ResultCallback<Status> {

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback locationCallback;
    private Marker geofenceMarker;

    private final float GEOFENCE_RADIUS = 100.0f;


    private String TAG = "MainActivity";
    private PendingIntent geoFencePendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Button btnCreateGeofence = findViewById(R.id.createGeofence);
        btnCreateGeofence.setOnClickListener(this);

        LinearLayout llSelectLocation = findViewById(R.id.select_location_layout);
        llSelectLocation.setOnClickListener(this);

        Button btnClearGeofence = findViewById(R.id.clearGeofence);
        btnClearGeofence.setOnClickListener(this);

        createGoogleApiClient();
        createLocationCallback();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        recoverMarker();
        Log.d(TAG,"onConnected");

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG,"onConnectionSuspended");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);
        getLastKnownLocation();

    }

    private void createGoogleApiClient() {
        if(googleApiClient == null){
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        createGeofenceMarker(latLng);
    }

    private void createGeofenceMarker(LatLng latLng) {

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng)
                .title(latLng.latitude + " : " + latLng.longitude)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

        geofenceMarker = mMap.addMarker(markerOptions);

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @SuppressLint("MissingPermission")
    public void getLastKnownLocation(){
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null) {
                    showMarkerOnCurrentLocation(location);
                }
                else
                {
                    startLocationUpdates();
                }
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                startLocationUpdates();
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)
                .setFastestInterval(800);
        mFusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback,null);
    }

    private void createLocationCallback(){
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if(locationResult == null)
                    return;

                for (Location location : locationResult.getLocations()){
                    showMarkerOnCurrentLocation(location);
                }
            }
        };
    }

    private void showMarkerOnCurrentLocation(Location location) {

        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

    }

    @Override
    public void onLocationChanged(Location location) {
        showMarkerOnCurrentLocation(location);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.select_location_layout) {
            try {
                if(geofenceMarker!=null) {
                    Geofence geofence = createGeofence(geofenceMarker.getPosition(),GEOFENCE_RADIUS );
                    GeofencingRequest geofenceRequest = createGeofenceRequest(geofence);
                    addGeofence(geofenceRequest);
                    LatLng latLng = geofenceMarker.getPosition();
                    GeofenceBean geofenceBean = new GeofenceBean(latLng.latitude,latLng.longitude);
                    AppDatabase.getAppDatabase(this).geofenceDAO().insert(geofenceBean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(v.getId() == R.id.clearGeofence)
            clearGeoFences();

    }

    private void clearGeoFences() {
        AppDatabase.getAppDatabase(this).geofenceDAO().delete();
    }

    private Geofence createGeofence(LatLng latLng , float radius) {
        Geofence geofence = null;
        try {
            geofence = new Geofence.Builder()
                    .setRequestId("Siddhant")
                    .setCircularRegion(latLng.latitude, latLng.longitude, radius)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                            | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return geofence;
    }

    private GeofencingRequest createGeofenceRequest(Geofence geofence){
        GeofencingRequest geoFencingRequest = null;
        try {
            geoFencingRequest = new GeofencingRequest.Builder()
                    .addGeofence(geofence)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return geoFencingRequest;


    }

    private PendingIntent createGeofencePendingIntent(){
        Intent intent = null;
        try {
            if(geoFencePendingIntent != null){
                return geoFencePendingIntent;
            }
//            intent = new Intent(this,GeofenceTransitionService.class);
            intent = new Intent(this,GeofenceBroadcast.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int GEOFENCE_REQ_CODE = 0;
//        return PendingIntent.getForegroundService(this, GEOFENCE_REQ_CODE,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        return PendingIntent.getBroadcast(this,GEOFENCE_REQ_CODE,intent,PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressLint("MissingPermission")
    private void addGeofence(GeofencingRequest geoFencingRequest){
        LocationServices.GeofencingApi.addGeofences(googleApiClient,geoFencingRequest,createGeofencePendingIntent()).setResultCallback(this);

        /*LocationServices.getGeofencingClient(this).
                addGeofences(geoFencingRequest,createGeofencePendingIntent()).
                addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                });*/
    }

    @Override
    public void onResult(@NonNull Status status) {
        if(status.isSuccess()){
            drawGeofence();
        }
    }

    private void drawGeofence() {

        CircleOptions circleOptions = new CircleOptions()
                .center( geofenceMarker.getPosition())
                .strokeColor(Color.argb(50, 70,70,70))
                .fillColor( Color.argb(100, 150,150,150) )
                    .radius(GEOFENCE_RADIUS );
        mMap.addCircle( circleOptions );
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void recoverMarker() {
        List<GeofenceBean> geofenceBeanList = AppDatabase.getAppDatabase(this).geofenceDAO().getAll();
        for(GeofenceBean geofenceBean : geofenceBeanList){
            LatLng latLng = new LatLng(geofenceBean.getLatitude(),geofenceBean.getLongitude());
            createGeofenceMarker(latLng);
            drawGeofence();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLastKnownLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }
}
