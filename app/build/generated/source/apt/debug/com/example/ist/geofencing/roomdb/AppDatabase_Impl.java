package com.example.ist.geofencing.roomdb;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class AppDatabase_Impl extends AppDatabase {
  private volatile GeofenceDAO _geofenceDAO;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `geofencesTable` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `latitude` REAL, `longitude` REAL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"54241d07d7217b6385456fee30e7699f\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `geofencesTable`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsGeofencesTable = new HashMap<String, TableInfo.Column>(3);
        _columnsGeofencesTable.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsGeofencesTable.put("latitude", new TableInfo.Column("latitude", "REAL", false, 0));
        _columnsGeofencesTable.put("longitude", new TableInfo.Column("longitude", "REAL", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysGeofencesTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesGeofencesTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoGeofencesTable = new TableInfo("geofencesTable", _columnsGeofencesTable, _foreignKeysGeofencesTable, _indicesGeofencesTable);
        final TableInfo _existingGeofencesTable = TableInfo.read(_db, "geofencesTable");
        if (! _infoGeofencesTable.equals(_existingGeofencesTable)) {
          throw new IllegalStateException("Migration didn't properly handle geofencesTable(com.example.ist.geofencing.roomdb.GeofenceBean).\n"
                  + " Expected:\n" + _infoGeofencesTable + "\n"
                  + " Found:\n" + _existingGeofencesTable);
        }
      }
    }, "54241d07d7217b6385456fee30e7699f", "4aaae4fb0632cbadd87f89fb1f11e23e");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "geofencesTable");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `geofencesTable`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public GeofenceDAO geofenceDAO() {
    if (_geofenceDAO != null) {
      return _geofenceDAO;
    } else {
      synchronized(this) {
        if(_geofenceDAO == null) {
          _geofenceDAO = new GeofenceDAO_Impl(this);
        }
        return _geofenceDAO;
      }
    }
  }
}
