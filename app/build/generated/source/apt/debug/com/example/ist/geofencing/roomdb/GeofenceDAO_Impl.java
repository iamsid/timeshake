package com.example.ist.geofencing.roomdb;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import java.lang.Double;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class GeofenceDAO_Impl implements GeofenceDAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfGeofenceBean;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public GeofenceDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfGeofenceBean = new EntityInsertionAdapter<GeofenceBean>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `geofencesTable`(`id`,`latitude`,`longitude`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, GeofenceBean value) {
        stmt.bindLong(1, value.getId());
        if (value.getLatitude() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindDouble(2, value.getLatitude());
        }
        if (value.getLongitude() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindDouble(3, value.getLongitude());
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM geofencesTable";
        return _query;
      }
    };
  }

  @Override
  public void insert(GeofenceBean geofenceBean) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfGeofenceBean.insert(geofenceBean);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<GeofenceBean> getAll() {
    final String _sql = "Select * from geofencesTable";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfLatitude = _cursor.getColumnIndexOrThrow("latitude");
      final int _cursorIndexOfLongitude = _cursor.getColumnIndexOrThrow("longitude");
      final List<GeofenceBean> _result = new ArrayList<GeofenceBean>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final GeofenceBean _item;
        final Double _tmpLatitude;
        if (_cursor.isNull(_cursorIndexOfLatitude)) {
          _tmpLatitude = null;
        } else {
          _tmpLatitude = _cursor.getDouble(_cursorIndexOfLatitude);
        }
        final Double _tmpLongitude;
        if (_cursor.isNull(_cursorIndexOfLongitude)) {
          _tmpLongitude = null;
        } else {
          _tmpLongitude = _cursor.getDouble(_cursorIndexOfLongitude);
        }
        _item = new GeofenceBean(_tmpLatitude,_tmpLongitude);
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
